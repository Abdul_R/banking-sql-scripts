REM   Script: banking SQL Query
REM   Banking App

CREATE TABLE Customer ( 
    cust_id VARCHAR(6), 
    cust_name VARCHAR(30) NOT NULL, 
    dob DATE NOT NULL, 
    Gender VARCHAR(11) NOT NULL, 
    City VARCHAR(15) NOT NULL, 
    CONSTRAINT Customer_cust_id_pk PRIMARY KEY(cust_id)  
   
);

desc Customer


desc Customer


SELECT GETDATE();

SELECT CURRENT_DATE FROM DUAL;

SELECT TIMESTAMP FROM DUAL;

SELECT CURRENT_TIMESTAMP() FROM DUAL;

SELECT CURRENT_TIMESTAMP FROM DUAL;

SELECT LOCALTIMESTAMP FROM DUAL;

CREATE TABLE Transaction( 
     trans_id VARCHAR(6), 
     trans_type VARCHAR(6), 
     trans_amount INT, 
     trans_dateTime DATETIME DEFAULT LOCALTIMESTAMP, 
     trans_from VARCHAR(6), 
     trans_to VARCHAR(6), 
     trans_status VARCHAR(12), 
     CONSTRAINT CHK_Transaction CHECK (trans_type in ('CREDIT','DEBIT')), 
     CONSTRAINT CHK_Transaction CHECK (trans_amount>1 AND trans_amount<10000), 
     CONSTRAINT CHK_Transaction CHECK (trans_status in ('NEW','IN PROGRESS','PROCESSED','PENDING','FAILED','RETRY')), 
     CONSTRAINT Transaction_trans_id_pk PRIMARY KEY(trans_id), 
     CONSTRAINT Transaction_trans_from_fk FOREIGN KEY(trans_from) REFERENCES Customer(cust_id), 
     CONSTRAINT Transaction_trans_to_fk FOREIGN KEY(trans_to) REFERENCES Customer(cust_id) 
 
     
 
);

CREATE TABLE Transaction( 
     trans_id VARCHAR(6), 
     trans_type VARCHAR(6), 
     trans_amount INT, 
     trans_dateTime TIMESTAMP WITH LOCAL TIME ZONE DEFAULT LOCALTIMESTAMP, 
     trans_from VARCHAR(6), 
     trans_to VARCHAR(6), 
     trans_status VARCHAR(12), 
     CONSTRAINT CHK_Transaction CHECK (trans_type in ('CREDIT','DEBIT')), 
     CONSTRAINT CHK_Transaction CHECK (trans_amount>1 AND trans_amount<10000), 
     CONSTRAINT CHK_Transaction CHECK (trans_status in ('NEW','IN PROGRESS','PROCESSED','PENDING','FAILED','RETRY')), 
     CONSTRAINT Transaction_trans_id_pk PRIMARY KEY(trans_id), 
     CONSTRAINT Transaction_trans_from_fk FOREIGN KEY(trans_from) REFERENCES Customer(cust_id), 
     CONSTRAINT Transaction_trans_to_fk FOREIGN KEY(trans_to) REFERENCES Customer(cust_id) 
 
     
 
);

CREATE TABLE Transaction( 
     trans_id VARCHAR(6), 
     trans_type VARCHAR(6), 
     trans_amount INT, 
     trans_dateTime TIMESTAMP WITH LOCAL TIME ZONE DEFAULT LOCALTIMESTAMP, 
     trans_from VARCHAR(6), 
     trans_to VARCHAR(6), 
     trans_status VARCHAR(12), 
     CONSTRAINT CHK_Transaction CHECK (trans_type in ('CREDIT','DEBIT')), 
     CONSTRAINT CHK_Transaction CHECK (trans_amount>1 AND trans_amount<10000), 
     CONSTRAINT CHK_Transaction CHECK (trans_status in ('NEW','IN PROGRESS','PROCESSED','PENDING','FAILED','RETRY')), 
     CONSTRAINT Transaction_trans_id_pk PRIMARY KEY(trans_id), 
     CONSTRAINT Transaction_tran_from_fk FOREIGN KEY(trans_from) REFERENCES Customer(cust_id), 
     CONSTRAINT Transaction_tran_to_fk FOREIGN KEY(trans_to) REFERENCES Customer(cust_id) 
 
     
 
);

CREATE TABLE Transactions( 
     trans_id VARCHAR(6), 
     trans_type VARCHAR(6), 
     trans_amount INT, 
     trans_dateTime TIMESTAMP WITH LOCAL TIME ZONE DEFAULT LOCALTIMESTAMP, 
     trans_from VARCHAR(6), 
     trans_to VARCHAR(6), 
     trans_status VARCHAR(12), 
     CONSTRAINT CHK_Transaction CHECK (trans_type in ('CREDIT','DEBIT')), 
     CONSTRAINT CHK_Transaction CHECK (trans_amount>1 AND trans_amount<10000), 
     CONSTRAINT CHK_Transaction CHECK (trans_status in ('NEW','IN PROGRESS','PROCESSED','PENDING','FAILED','RETRY')), 
     CONSTRAINT Transaction_trans_id_pk PRIMARY KEY(trans_id), 
     CONSTRAINT Transaction_tran_from_fk FOREIGN KEY(trans_from) REFERENCES Customer(cust_id), 
     CONSTRAINT Transaction_tran_to_fk FOREIGN KEY(trans_to) REFERENCES Customer(cust_id) 
 
     
 
);

CREATE TABLE Transactions( 
     trans_id VARCHAR(6), 
     trans_type VARCHAR(6), 
     trans_amount INT, 
     trans_dateTime TIMESTAMP WITH LOCAL TIME ZONE DEFAULT LOCALTIMESTAMP, 
     trans_from VARCHAR(6), 
     trans_to VARCHAR(6), 
     trans_status VARCHAR(12), 
     CONSTRAINT CHK_Transaction1 CHECK (trans_type in ('CREDIT','DEBIT')), 
     CONSTRAINT CHK_Transaction2 CHECK (trans_amount>1 AND trans_amount<10000), 
     CONSTRAINT CHK_Transaction3 CHECK (trans_status in ('NEW','IN PROGRESS','PROCESSED','PENDING','FAILED','RETRY')), 
     CONSTRAINT Transaction_trans_id_pk PRIMARY KEY(trans_id), 
     CONSTRAINT Transaction_tran_from_fk FOREIGN KEY(trans_from) REFERENCES Customer(cust_id), 
     CONSTRAINT Transaction_tran_to_fk FOREIGN KEY(trans_to) REFERENCES Customer(cust_id) 
 
     
 
);

desc Transactions 


INSERT INTO Customer 
VALUES('C00001','Abdul','1998-04-21','male','Madurai');

INSERT INTO Customer 
VALUES('C00001','Abdul','21-Apr-1998','male','Madurai');

INSERT INTO Customer 
VALUES('C00001','Abdul','21-Apr-1998','male','Madurai');

INSERT INTO Customer 
VALUES('C00002','Hussain','11-Mar-1995','male','Chennai');

INSERT INTO Customer 
VALUES('C00003','Areif','20-Feb-1998','male','Trichy');

INSERT INTO Customer 
VALUES('C00004','Priya','10-Apr-1997','female','coimbatore');

INSERT INTO Customer 
VALUES('C00005','Saranya','13-Feb-1999','female','Madurai');

INSERT INTO Customer 
VALUES('C00006','Sowndharya','21-Aug-1998','female','Tiruppur');

INSERT INTO Customer 
VALUES('C00007','Puspa','21-Jun-1995','female','Chennai');

INSERT INTO Customer 
VALUES('C00008','Selva','22-Apr-1999','male','Viruthu Nagar');

SELECT * FROM Customer;

INSERT INTO Transactions (trans_id,trans_type,trans_amount,trans_from,trans_to,trans_status) 
VALUES ('A00001','CREDIT',1000,'C00001','C00002','NEW');

select * from Transactions;

VALUES ('A00002','CREDIT',100,'C00001','C00003','IN PROGRESS'); 


INSERT INTO Transactions (trans_id,trans_type,trans_amount,trans_from,trans_to,trans_status) 
VALUES ('A00002','CREDIT',100,'C00001','C00003','IN PROGRESS');

select * from Transactions;

CREATE TABLE Customer ( 
    cust_id VARCHAR(6), 
    cust_name VARCHAR(30) NOT NULL, 
    dob DATE NOT NULL, 
    Gender VARCHAR(11) NOT NULL, 
    City VARCHAR(15) NOT NULL, 
    CONSTRAINT Customer_cust_id_pk PRIMARY KEY(cust_id)  
   
);

desc Customer


CREATE TABLE Transactions( 
     trans_id VARCHAR(6), 
     trans_type VARCHAR(6), 
     trans_amount INT, 
     trans_dateTime TIMESTAMP WITH LOCAL TIME ZONE DEFAULT LOCALTIMESTAMP, 
     trans_from VARCHAR(6), 
     trans_to VARCHAR(6), 
     trans_status VARCHAR(12), 
     CONSTRAINT CHK_Transaction1 CHECK (trans_type in ('CREDIT','DEBIT')), 
     CONSTRAINT CHK_Transaction2 CHECK (trans_amount>1 AND trans_amount<10000), 
     CONSTRAINT CHK_Transaction3 CHECK (trans_status in ('NEW','IN PROGRESS','PROCESSED','PENDING','FAILED','RETRY')), 
     CONSTRAINT Transaction_trans_id_pk PRIMARY KEY(trans_id), 
     CONSTRAINT Transaction_tran_from_fk FOREIGN KEY(trans_from) REFERENCES Customer(cust_id), 
     CONSTRAINT Transaction_tran_to_fk FOREIGN KEY(trans_to) REFERENCES Customer(cust_id) 
 
);

desc Transactions 


INSERT INTO Customer 
VALUES('C00001','Abdul','21-Apr-1998','male','Madurai');

INSERT INTO Customer 
VALUES('C00002','Hussain','11-Mar-1995','male','Chennai');

INSERT INTO Customer 
VALUES('C00003','Areif','20-Feb-1998','male','Trichy');

INSERT INTO Customer 
VALUES('C00004','Priya','10-Apr-1997','female','coimbatore');

INSERT INTO Customer 
VALUES('C00005','Saranya','13-Feb-1999','female','Madurai');

INSERT INTO Customer 
VALUES('C00006','Sowndharya','21-Aug-1998','female','Tiruppur');

INSERT INTO Customer 
VALUES('C00007','Puspa','21-Jun-1995','female','Chennai');

INSERT INTO Customer 
VALUES('C00008','Selva','22-Apr-1999','male','Viruthu Nagar');

SELECT * FROM Customer;

SELECT LOCALTIMESTAMP FROM DUAL;

INSERT INTO Transactions (trans_id,trans_type,trans_amount,trans_from,trans_to,trans_status) 
VALUES ('A00001','CREDIT',1000,'C00001','C00002','NEW');

select * from Transactions;

INSERT INTO Transactions (trans_id,trans_type,trans_amount,trans_from,trans_to,trans_status) 
VALUES ('A00002','CREDIT',100,'C00001','C00003','IN PROGRESS');

select * from Transactions;

INSERT INTO Transactions (trans_id,trans_type,trans_amount,trans_from,trans_to,trans_status) 
VALUES ('A00002','CREDIT',100,'C00001','C00003','IN PROGRESS');

INSERT INTO Transactions (trans_id,trans_type,trans_amount,trans_from,trans_to,trans_status) 
VALUES ('A00002','CREDIT',100,'C00001','C00003','IN PROGRESS');

INSERT INTO Transactions (trans_id,trans_type,trans_amount,trans_from,trans_to,trans_status) 
VALUES ('A00002','CREDIT',100,'C00001','C00003','IN PROGRESS');

INSERT INTO Transactions (trans_id,trans_type,trans_amount,trans_from,trans_to,trans_status) 
VALUES ('A00002','CREDIT',100,'C00001','C00003','IN PROGRESS');

CREATE TABLE Customer ( 
    cust_id VARCHAR(6), 
    cust_name VARCHAR(30) NOT NULL, 
    dob DATE NOT NULL, 
    Gender VARCHAR(11) NOT NULL, 
    City VARCHAR(15) NOT NULL, 
    CONSTRAINT Customer_cust_id_pk PRIMARY KEY(cust_id)  
   
);

desc Customer


CREATE TABLE Transactions( 
     trans_id VARCHAR(6), 
     trans_type VARCHAR(6), 
     trans_amount INT, 
     trans_dateTime TIMESTAMP WITH LOCAL TIME ZONE DEFAULT LOCALTIMESTAMP, 
     trans_from VARCHAR(6), 
     trans_to VARCHAR(6), 
     trans_status VARCHAR(12), 
     CONSTRAINT CHK_Transaction1 CHECK (trans_type in ('CREDIT','DEBIT')), 
     CONSTRAINT CHK_Transaction2 CHECK (trans_amount>1 AND trans_amount<10000), 
     CONSTRAINT CHK_Transaction3 CHECK (trans_status in ('NEW','IN PROGRESS','PROCESSED','PENDING','FAILED','RETRY')), 
     CONSTRAINT Transaction_trans_id_pk PRIMARY KEY(trans_id), 
     CONSTRAINT Transaction_tran_from_fk FOREIGN KEY(trans_from) REFERENCES Customer(cust_id), 
     CONSTRAINT Transaction_tran_to_fk FOREIGN KEY(trans_to) REFERENCES Customer(cust_id) 
 
);

desc Transactions 


INSERT INTO Customer 
VALUES('C00001','Abdul','21-Apr-1998','male','Madurai');

INSERT INTO Customer 
VALUES('C00002','Hussain','11-Mar-1995','male','Chennai');

INSERT INTO Customer 
VALUES('C00003','Areif','20-Feb-1998','male','Trichy');

INSERT INTO Customer 
VALUES('C00004','Priya','10-Apr-1997','female','coimbatore');

INSERT INTO Customer 
VALUES('C00005','Saranya','13-Feb-1999','female','Madurai');

INSERT INTO Customer 
VALUES('C00006','Sowndharya','21-Aug-1998','female','Tiruppur');

INSERT INTO Customer 
VALUES('C00007','Puspa','21-Jun-1995','female','Chennai');

INSERT INTO Customer 
VALUES('C00008','Selva','22-Apr-1999','male','Viruthu Nagar');

SELECT * FROM Customer;

SELECT LOCALTIMESTAMP FROM DUAL;

INSERT INTO Transactions (trans_id,trans_type,trans_amount,trans_from,trans_to,trans_status) 
VALUES ('A00001','CREDIT',1000,'C00001','C00002','NEW');

select * from Transactions;

INSERT INTO Transactions (trans_id,trans_type,trans_amount,trans_from,trans_to,trans_status) 
VALUES ('A00002','CREDIT',100,'C00001','C00003','IN PROGRESS');

select * from Transactions;

INSERT INTO Transactions (trans_id,trans_type,trans_amount,trans_from,trans_to,trans_status) 
VALUES ('A00002','CREDIT',100,'C00001','C00003','IN PROGRESS');

INSERT INTO Transactions (trans_id,trans_type,trans_amount,trans_from,trans_to,trans_status) 
VALUES ('A00002','CREDIT',100,'C00001','C00003','IN PROGRESS');

INSERT INTO Transactions (trans_id,trans_type,trans_amount,trans_from,trans_to,trans_status) 
VALUES ('A00002','CREDIT',100,'C00001','C00003','IN PROGRESS');

INSERT INTO Transactions (trans_id,trans_type,trans_amount,trans_from,trans_to,trans_status) 
VALUES ('A00002','CREDIT',100,'C00001','C00003','IN PROGRESS');INSERT INTO Transactions (trans_id,trans_type,trans_amount,trans_from,trans_to,trans_status) 
VALUES ('A00002','CREDIT',100,'C00001','C00003','IN PROGRESS');

INSERT INTO Transactions (trans_id,trans_type,trans_amount,trans_from,trans_to,trans_status) 
VALUES ('A00003','CREDIT',500,'C00001','C00004','PROCESSED');

INSERT INTO Transactions (trans_id,trans_type,trans_amount,trans_from,trans_to,trans_status) 
VALUES ('A00004','CREDIT',2000,'C00001','C00005','PENDING');

INSERT INTO Transactions (trans_id,trans_type,trans_amount,trans_from,trans_to,trans_status) 
VALUES ('A00005','CREDIT',3000,'C00001','C00006','FAILED');

INSERT INTO Transactions (trans_id,trans_type,trans_amount,trans_from,trans_to,trans_status) 
VALUES ('A00006','CREDIT',4000,'C00001','C00007','RETRY');

INSERT INTO Transactions (trans_id,trans_type,trans_amount,trans_from,trans_to,trans_status) 
VALUES ('A00007','CREDIT',400,'C00001','C00008','RETRY');

INSERT INTO Transactions (trans_id,trans_type,trans_amount,trans_from,trans_to,trans_status) 
VALUES ('A00008','DEBIT',400,'C00001','C00001','NEW');

INSERT INTO Transactions (trans_id,trans_type,trans_amount,trans_from,trans_to,trans_status) 
VALUES ('A00009','DEBIT',1000,'C00002','C00002','IN PROGRESS');

INSERT INTO Transactions (trans_id,trans_type,trans_amount,trans_from,trans_to,trans_status) 
VALUES ('A00010','DEBIT',2000,'C00003','C00003','PROCESSED');

INSERT INTO Transactions (trans_id,trans_type,trans_amount,trans_from,trans_to,trans_status) 
VALUES ('A00011','DEBIT',3000,'C00004','C00004','PENDING');

INSERT INTO Transactions (trans_id,trans_type,trans_amount,trans_from,trans_to,trans_status) 
VALUES ('A00012','DEBIT',3000,'C00005','C00005','FAILED');

INSERT INTO Transactions (trans_id,trans_type,trans_amount,trans_from,trans_to,trans_status) 
VALUES ('A00013','DEBIT',4000,'C00006','C00006','RETRY');

INSERT INTO Transactions (trans_id,trans_type,trans_amount,trans_from,trans_to,trans_status) 
VALUES ('A00014','DEBIT',4000,'C00007','C00007','PENDING');

INSERT INTO Transactions (trans_id,trans_type,trans_amount,trans_from,trans_to,trans_status) 
VALUES ('A00015','DEBIT',5000,'C00008','C00008','PROCESSED');

INSERT INTO Transactions (trans_id,trans_type,trans_amount,trans_from,trans_to,trans_status) 
VALUES ('A00016','CREDIT',100,'C00001','C00002','NEW');

select * from Transactions;

INSERT INTO Transactions (trans_id,trans_type,trans_amount,trans_from,trans_to,trans_status) 
VALUES ('A00017','CREDIT',101,'C00001','C00003','IN PROGRESS');

select * from Transactions;

INSERT INTO Transactions (trans_id,trans_type,trans_amount,trans_from,trans_to,trans_status) 
VALUES ('A00018','CREDIT',200,'C00001','C00004','PROCESSED');

INSERT INTO Transactions (trans_id,trans_type,trans_amount,trans_from,trans_to,trans_status) 
VALUES ('A00019','CREDIT',220,'C00001','C00005','PENDING');

INSERT INTO Transactions (trans_id,trans_type,trans_amount,trans_from,trans_to,trans_status) 
VALUES ('A00020','CREDIT',250,'C00001','C00006','FAILED');

INSERT INTO Transactions (trans_id,trans_type,trans_amount,trans_from,trans_to,trans_status) 
VALUES ('A00021','CREDIT',210,'C00001','C00007','RETRY');

INSERT INTO Transactions (trans_id,trans_type,trans_amount,trans_from,trans_to,trans_status) 
VALUES ('A00022','CREDIT',400,'C00001','C00008','RETRY');

INSERT INTO Transactions (trans_id,trans_type,trans_amount,trans_from,trans_to,trans_status) 
VALUES ('A00023','DEBIT',400,'C00001','C00001','NEW');

INSERT INTO Transactions (trans_id,trans_type,trans_amount,trans_from,trans_to,trans_status) 
VALUES ('A00024','DEBIT',1000,'C00002','C00002','IN PROGRESS');

INSERT INTO Transactions (trans_id,trans_type,trans_amount,trans_from,trans_to,trans_status) 
VALUES ('A00025','DEBIT',2000,'C00003','C00003','PROCESSED');

INSERT INTO Transactions (trans_id,trans_type,trans_amount,trans_from,trans_to,trans_status) 
VALUES ('A00026','DEBIT',3000,'C00004','C00004','PENDING');

INSERT INTO Transactions (trans_id,trans_type,trans_amount,trans_from,trans_to,trans_status) 
VALUES ('A00027','DEBIT',3000,'C00005','C00005','FAILED');

INSERT INTO Transactions (trans_id,trans_type,trans_amount,trans_from,trans_to,trans_status) 
VALUES ('A00028','DEBIT',4000,'C00006','C00006','RETRY');

INSERT INTO Transactions (trans_id,trans_type,trans_amount,trans_from,trans_to,trans_status) 
VALUES ('A00029','DEBIT',4000,'C00007','C00007','PENDING');

INSERT INTO Transactions (trans_id,trans_type,trans_amount,trans_from,trans_to,trans_status) 
VALUES ('A00030','DEBIT',5000,'C00008','C00008','PROCESSED');

select * from Transactions;

INSERT INTO Customer 
VALUES('C00009','Selva pandi','22-Apr-1960','male','Viruthu Nagar');

INSERT INTO Customer 
VALUES('C00010','Puspa Raj','21-Jun-1959','male','Chennai');

SELECT cust_id,cust_name,Gender,City 
 
FROM Customer 
 
WHERE DATEDIFF(YEAR, dob, GETDATE())>18;

Select cust_id,cust_name,Gender,City 
from Customer 
where trunc(sysdate-emp.dob) > 18:;

Select cust_id,cust_name,Gender,City 
from Customer 
where trunc(sysdate-emp.dob) > 18;

Select cust_id,cust_name,Gender,City 
from Customer 
where trunc(sysdate-dob) > 18;

Select cust_id,cust_name,Gender,City 
from Customer 
where trunc(sysdate-dob) <23;

Select cust_id,cust_name,Gender,City 
from Customer 
where trunc(sysdate-dob) >28;

Select cust_id,cust_name,Gender,City 
from Customer 
where trunc(sysdate-dob) >30;

SELECT * 
from ( 
    select  
    cust_id, 
    cust_name, 
    Gender, 
    City, 
    trunc (month_between(sysdate,dob)/12)as "age" 
    from Customer 
  
) 
where "age">18 and "age"<32;

SELECT * 
from ( 
    select  
    cust_id, 
    cust_name, 
    Gender, 
    City, 
    trunc (months_between(sysdate,dob)/12)as "age" 
    from Customer 
  
) 
where "age">18 and "age"<32;

SELECT Customer.* 
FROM Customer 
WHERE (SELECT COUNT(*) FROM Customer AS c GROUP BY CITY HAVING CITY = Customer.CITY) > 1;

select  cust_id, 
    cust_name, 
    City  
    from Customer  
    Where City= "Madurai";

select  cust_id, 
    cust_name, 
    City  
    from Customer  
    Where City= 'Madurai';

select * from Customer 
where city in ( 
    select city from Customer group by city having count(city) > 1 
);

select city from Customer group by city having count(city) = 1;

select * from Transactions where  trans_amount<250;

select * from Transactions where  trans_amount>500 and trans_type='CREDIT' ;

select * from Transactions where  trans_status='IN PROGRESS';

select * from Transactions where  trans_status='PENDING' and trans_status='RETRY' ;

select * from Transactions where  trans_status='PENDING' or trans_status='RETRY';

select * from Transactions where  trans_status='NEW'and trans_type='DEBIT';

select distinct c.* from Customer c 
inner join Transactions t  
    on t.trans_from = c. cust_id 
    and t. trans_type = 'DEBIT' 
where c.city = 'Madurai';

select distinct c.* from Customer c 
inner join Transactions t  
    on t.trans_from = c. cust_id 
    and t. trans_type = 'DEBIT' 
where c.city = 'Chennai';

select  c.*,t.* from Customer c 
inner join Transactions t  
    on t.trans_from = c. cust_id 
    and t. trans_status = 'FAILED' 
where trunc (months_between(sysdate,c.dob)/12)>30;

INSERT INTO Transactions (trans_id,trans_type,trans_amount,trans_from,trans_to,trans_status) 
VALUES ('A00030','DEBIT',5000,'C00010','C00008','FAILED');

INSERT INTO Transactions (trans_id,trans_type,trans_amount,trans_from,trans_to,trans_status) 
VALUES ('A00031','DEBIT',5000,'C00010','C00008','FAILED');

select  c.*,t.* from Customer c 
inner join Transactions t  
    on t.trans_from = c. cust_id 
    and t. trans_status = 'FAILED' 
where trunc (months_between(sysdate,c.dob)/12)>30;

select  c.*,t.* from Customer c 
inner join Transactions t  
    on t.trans_from = c. cust_id 
    and t. trans_type = 'DEBIT' 
    and t. trans_status in ('NEW','RETRY') 
where c.Gender='Female' ;

select  c.*,t.* from Customer c 
inner join Transactions t  
    on t.trans_from = c. cust_id 
    and t. trans_type = 'DEBIT' 
    and t. trans_status in ('NEW','RETRY') 
where c.Gender='Male' ;

select  c.*,t.* from Customer c 
inner join Transactions t  
    on t.trans_from = c. cust_id 
    and t. trans_type = 'DEBIT' 
    and t. trans_status ='NEW' 
where c.Gender='Male' ;

select * from Transactions;

select  c.*,t.* from Customer c 
inner join Transactions t  
    on t.trans_from = c. cust_id 
    and t. trans_type = 'DEBIT' 
    and t. trans_status ='NEW' 
where c.Gender='female' ;

select  c.*,t.* from Customer c 
inner join Transactions t  
    on t.trans_from = c. cust_id 
    and t. trans_type = 'DEBIT' 
    and t. trans_status ='RETRY' 
where c.Gender='female';

select  c.*,t.* from Customer c 
inner join Transactions t  
    on t.trans_from = c. cust_id 
    and t. trans_type = 'DEBIT' 
    and t. trans_status in ('RETRY', 'NEW') 
where c.Gender='female';

select  c.*,t.* from Customer c 
inner join Transactions t  
    on t.trans_from = c. cust_id 
    and t. trans_type = 'DEBIT' 
    and t. trans_status in ('RETRY', 'NEW') 
where c.Gender='female';

